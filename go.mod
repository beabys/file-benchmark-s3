module gitlab.com/beabys/file-benchmark-s3

go 1.16

require (
	github.com/aws/aws-sdk-go v1.43.18
	github.com/briandowns/spinner v1.16.0
	github.com/mattn/go-colorable v0.1.11 // indirect
	github.com/spf13/cobra v1.2.1
	gopkg.in/ini.v1 v1.62.0
)
